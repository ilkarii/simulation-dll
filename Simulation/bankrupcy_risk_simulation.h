#pragma once
#include <stdexcept>
#include <random>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <chrono>
using namespace std;

namespace SimulationFuncs
{
	extern "C" { __declspec(dllexport) int r_value(int N, double p); }
	extern "C" { __declspec(dllexport) int T_value(double m, double d, int N, double p); }
	extern "C" { __declspec(dllexport) double bankruptcy_probability(double m, double d, int N, double p, int L, int U, int P,double eps); }
	extern "C" { __declspec(dllexport) void U_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int inc_U, double inc_m, int tests, double eps); }
	extern "C" { __declspec(dllexport) void U_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int inc_U, double inc_d, int tests, double eps); }
	extern "C" { __declspec(dllexport) void P_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int inc_P, int inc_N, int tests, double eps); }
	extern "C" { __declspec(dllexport) void P_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int inc_P, double inc_p, int tests, double eps); }
	extern "C" { __declspec(dllexport) void P_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int inc_P, double inc_m, int tests, double eps); }
	extern "C" { __declspec(dllexport) void P_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int inc_P, double inc_d, int tests, double eps); }
	extern "C" { __declspec(dllexport) void N_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int inc_N, double inc_m, int tests, double eps); }
	extern "C" { __declspec(dllexport) void N_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int inc_N, double inc_d, int tests, double eps); }
	extern "C" { __declspec(dllexport) void p_and_m_variation(double m, double d, int N, double p, int L, int U, int P, double inc_p, double inc_m, int tests, double eps); }
	extern "C" { __declspec(dllexport) void p_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double inc_p, double inc_d, int tests, double eps); }
	extern "C" { __declspec(dllexport) void m_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double inc_m, double inc_d, int tests, double eps); }
	extern "C" { __declspec(dllexport) void U_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int inc_U, double inc_p, int tests, double eps); }
	extern "C" { __declspec(dllexport) void U_and_P_variation(double m, double d, int N, double p, int L, int U, int P, int inc_U, int inc_P, int tests, double eps); }
	extern "C" { __declspec(dllexport) void N_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int inc_N, double inc_p, int tests, double eps); }
	extern "C" { __declspec(dllexport) void U_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int inc_U, int inc_N, int tests, double eps); }
}