#include "bankrupcy_risk_simulation.h";

namespace SimulationFuncs
{
	#define MODLUS 2147483647   
	#define MULT1       24112   
	#define MULT2       26143  
	double lcgrand(long stream)
	{
		long zi, lowprd, hi31;

		zi = stream;
		lowprd = (zi & 65535) * MULT1;
		hi31 = (zi >> 16) * MULT1 + (lowprd >> 16);
		zi = ((lowprd & 65535) - MODLUS) +
			((hi31 & 32767) << 16) + (hi31 >> 15);
		if (zi < 0) zi += MODLUS;
		lowprd = (zi & 65535) * MULT2;
		hi31 = (zi >> 16) * MULT2 + (lowprd >> 16);
		zi = ((lowprd & 65535) - MODLUS) +
			((hi31 & 32767) << 16) + (hi31 >> 15);
		if (zi < 0) zi += MODLUS;
		return (zi >> 7 | 1) / 16777216.0;
	}
	double CND(double d)
	{
		const double       A1 = 0.31938153;
		const double       A2 = -0.356563782;
		const double       A3 = 1.781477937;
		const double       A4 = -1.821255978;
		const double       A5 = 1.330274429;
		const double RSQRT2PI = 0.39894228040143267793994605993438;

		double
			K = 1.0 / (1.0 + 0.2316419 * fabs(d));

		double
			cnd = RSQRT2PI * exp(-0.5 * d * d) *
			(K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));

		if (d > 0)
			cnd = 1.0 - cnd;

		return cnd;
	}

	double analytic_bankruptcy_probability(double m, double d, int N, double p, int U, int P) {
		double x = 1.0;
		x = (double)((U + P*N*p*m)) / pow((double)(N*p*(d + m*m*(1 - p))), 0.5);
		x = 1.0 - CND(x);
		return x;
	}
	void to_file(string name, vector<vector<long double>> & vec) {
		ofstream out;
		out.open(name);
		for (size_t i = 0; i < vec.size(); i++)
		{
			for (size_t j = 0; j < vec[0].size() - 1; j++)
			{
				out << vec[i][j] << " ";
			}
			out << vec[i][vec[i].size() - 1];
			out << endl;
		}
		out.close();
		return;
	}
	void to_csv(string name, vector<vector<long double>> & vec, string header, string info, string increment) {
		ofstream out;
		out.open(name);
		out << header;
		for (size_t i = 0; i < vec.size(); i++)
		{
			for (size_t j = 0; j < vec[0].size() - 1; j++)
			{
				out << vec[i][j] << ";";
			}
			out << vec[i][vec[i].size() - 1];
			out << endl;
		}
		out.close();
		out.open(name.insert(name.size() - 4, "_info"));
		out << info;
		out << increment;
		out.close();
		return;
	}
	void U_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, double max_m, int tests, double eps){
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_m = (max_m - m) / tests;
		int inc_U = (max_U - U) / tests;
		double m_t = m;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)U;
				matrix[k][1] = (double)m;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				m += inc_m;
			}
			U += inc_U;
			m = m_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "U;m;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_m\n";
		increment += to_string(inc_U) + ";" + to_string(inc_m) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void U_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, double max_d, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_d = (max_d - d) / tests;
		int inc_U = (max_U - U) / tests;
		double d_t = d;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)U;
				matrix[k][1] = (double)d;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				d += inc_d;
			}
			U += inc_U;
			d = d_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "U;d;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_d\n";
		increment += to_string(inc_U) + ";" + to_string(inc_d) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void P_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, int max_N, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		int inc_N = (max_N - N) / tests;
		int inc_P = (max_P - P) / tests;
		int N_t = N;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)P;
				matrix[k][1] = (double)N;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				N += inc_N;
			}
			P += inc_P;
			N = N_t;
		}
		to_file("data.txt", matrix);
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "P;N;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_P;inc_N\n";
		increment += to_string(inc_P) + ";" + to_string(inc_N) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
	}
	void P_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, double max_p, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_p = (max_p - p) / tests;
		int inc_P = (max_P - P) / tests;
		double p_t = p;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)P;
				matrix[k][1] = (double)p;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				if (p + inc_p <= 1.0)
					p += inc_p;
			}
			P += inc_P;
			p = p_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "P;p;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_P;inc_p\n";
		increment += to_string(inc_P) + ";" + to_string(inc_p) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt",matrix);
	}
	void P_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_P,double max_m, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_m = (max_m - m) / tests;
		int inc_P = (max_P - P) / tests;
		double m_t = m;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)P;
				matrix[k][1] = (double)m;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				m += inc_m;
			}
			P += inc_P;
			m = m_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "P;m;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_P;inc_m\n";
		increment += to_string(inc_P) + ";" + to_string(inc_m) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void P_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_P, double max_d, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_d = (max_d - d) / tests;
		int inc_P = (max_P - P) / tests;
		double d_t = d;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)P;
				matrix[k][1] = (double)d;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				d += inc_d;
			}
			P += inc_P;
			d = d_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "P;d;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_P;inc_d\n";
		increment += to_string(inc_P) + ";" + to_string(inc_d) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void N_and_m_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_m, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_m = (max_m - m) / tests;
		int inc_N = (max_N - N) / tests;
		double m_t = m;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)N;
				matrix[k][1] = (double)m;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				m += inc_m;
			}
			N += inc_N;
			m = m_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "N;m;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_N;inc_m\n";
		increment += to_string(inc_N) + ";" + to_string(inc_m) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void N_and_d_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_d, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_d = (max_d - d) / tests;
		int inc_N = (max_N - N) / tests;
		double d_t = d;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)N;
				matrix[k][1] = (double)d;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				d += inc_d;
			}
			N += inc_N;
			d = d_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "N;d;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_N;inc_d\n";
		increment += to_string(inc_N) + ";" + to_string(inc_d) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void p_and_m_variation(double m, double d, int N, double p, int L, int U, int P, double max_p, double max_m, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_m = (max_m - m) / tests;
		double inc_p = (max_p - p) / tests;
		double m_t = m;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)p;
				matrix[k][1] = (double)m;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				m += inc_m;
			}
			if (p + inc_p <= 1.0)
				p += inc_p;
			m = m_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "p;m;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_p;inc_m\n";
		increment += to_string(inc_p) + ";" + to_string(inc_m) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void p_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double max_p, double max_d, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_p = (max_p - p) / tests;
		double inc_d = (max_d - d) / tests;
		double d_t = d;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)p;
				matrix[k][1] = (double)d;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				d += inc_d;
			}
			if (p + inc_p <= 1.0)
				p += inc_p;
			d = d_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "p;d;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_p;inc_d\n";
		increment += to_string(inc_p) + ";" + to_string(inc_d) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void m_and_d_variation(double m, double d, int N, double p, int L, int U, int P, double max_m, double max_d, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_m = (max_m - m) / tests;
		double inc_d = (max_d - d) / tests;
		double d_t = d;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)m;
				matrix[k][1] = (double)d;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				d += inc_d;
			}
			m += inc_m;
			d = d_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "m;d;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_m\n";
		increment += to_string(inc_m) + ";" + to_string(inc_d) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void U_and_p_variation(double m, double d, int N, double p, int L, int U, int P,int max_U,double max_p,int tests,double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_p = (max_p - p) / tests;
		int inc_U = (max_U - U) / tests;
		double p_t = p;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (long double)U;
				matrix[k][1] = (long double)p;
				matrix[k][2] = bankruptcy_probability(m,d,N,p,L,U,P,eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				if (p + inc_p <= 1.0)
					p += inc_p;
			}
			U += inc_U;
			p = p_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "U;p;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_p\n";
		increment += to_string(inc_U) + ";" + to_string(inc_p) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void U_and_P_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, int max_P, int tests, double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		int inc_P = (max_P - P) / tests;
		int inc_U = (max_U - U) / tests;
		int P_t = P;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (double)U;
				matrix[k][1] = (double)P;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P, eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				P += inc_P;
			}
			U += inc_U;
			P = P_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "U;P;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_P\n";
		increment += to_string(inc_U) + ";" + to_string(inc_P) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void N_and_p_variation(double m, double d, int N, double p, int L, int U, int P, int max_N, double max_p, int tests,double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		double inc_p = (max_p - p) / tests;
		int inc_N = (max_N - N) / tests;
		double p_t = p;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (long double)N;
				matrix[k][1] = (long double)p;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P,eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
				if (p + inc_p <= 1.0)
					p += inc_p;
			}
			N += inc_N;
			p = p_t;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "N;p;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_N;inc_p\n";
		increment += to_string(inc_N) + ";" + to_string(inc_p) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	void U_and_N_variation(double m, double d, int N, double p, int L, int U, int P, int max_U, int max_N, int tests,double eps) {
		vector<vector<long double>> matrix(tests*tests);
		vector<long double> v3(4);
		for (size_t i = 0; i < matrix.size(); i++) {
			matrix[i] = v3;
		}
		int inc_N = (max_N - N) / tests;
		int inc_U = (max_U - U) / tests;
		double p_t = p;
		for (int i = 0; i < tests; i++) {
			for (int j = 0; j < tests; j++) {
				int k = i*tests + j;
				matrix[k][0] = (long double)N;
				matrix[k][1] = (long double)U;
				matrix[k][2] = bankruptcy_probability(m, d, N, p, L, U, P,eps);
				matrix[k][3] = analytic_bankruptcy_probability(m, d, N, p, U, P);
			}
			N += inc_N;
			U += inc_U;
		}
		string header = "";
		string increment = "";
		string info;
		info = "\n\n\dziritadi monacemebi\n";
		info += "N;U;P;m;d;p;L;tests;eps\n";
		info += to_string(N) + ";" + to_string(U) + ";" + to_string(P) + ";" + to_string(m) + ";" + to_string(d) +
			";" + to_string(p) + ";" + to_string(L) + ";" + to_string(tests) + ";" + to_string(eps) + "\n";
		header = "U;N;p[b];F[b]\n";
		increment = "\n\n\cvladebis inkrementi\n";
		increment += "inc_U;inc_N\n";
		increment += to_string(inc_U) + ";" + to_string(inc_N) + "\n";
		to_csv("data.csv", matrix, header, info, increment);
		to_file("data.txt", matrix);
	}
	int r_value(int N,double p) {
		int r = 0;
		long seed;
		for (int i = 0; i < N; ++i) {
			seed = chrono::system_clock::now().time_since_epoch().count();
			if (lcgrand(seed)>p) ++r;
		}
		return r;
	}
	int T_value(double m,double d,int N,double p) {
		int r = r_value(N,p);
		int t = 0;
		
		default_random_engine generator;
		normal_distribution<long double> distribution(m, d);

		for (int i = 0; i<r; ++i) {
			long double number = distribution(generator);
			t += (int)number;
		}
		return t;
	}
	double bankruptcy_probability(double m,double d,int N,double p,int L, int U,int P,double eps) {
		long double old_probability = 0.0 - 2 * eps;
		long double new_probability = 0.0;
		do {
			int counter = 0;
			int T;
			for (unsigned int i = 0; i < L; i++) {
				T = T_value(m, d, N, p);
				if (U + P < T) counter++;
			}
			old_probability = new_probability;
			new_probability = (long double)counter / L;
			L <<= 1;
		}while(L<(1 << 13) || fabs(old_probability - new_probability) > eps);
		return new_probability;
	}

}